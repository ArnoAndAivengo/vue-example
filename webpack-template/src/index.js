// JS
import './js/'

// SCSS
import './assets/scss/main.scss'

// CSS (example)
// import './assets/css/main.css'

// Vue.js
window.Vue = require('vue')

// vuex
import store from './store'

// Vue components (for use in html)
Vue.component('app', require('./App.vue').default)

// Vue init
const app = new Vue({
  store,
  el: '#app',
})
