// Default
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// Pages
import Home from '@/pages/Home'
import Example from '@/pages/Shop'
import Product from '@/pages/Product'
import NotFound from '@/pages/404'

// Routering
export default new Router ( {
  // mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/shop',
      name: 'shop',
      component: Example
    },
    {
      path: '/shop/:id',
      name: 'product',
      component: Product
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})
