export default {
  state: {
    shopList: [
      {
        id: 1,
        title: 'Nike Red',
        descr: 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла")',
        img: require('../assets/img/1.png'),
        gallery: [
          {
            name: 'Nike boots First',
            img: require('../assets/img/1.png'),
          },
          {
            name: 'Nike boots Second',
            img: require('../assets/img/2.png'),
          },
          {
            name: 'Nike boots Third',
            img: require('../assets/img/3.png'),
          }
        ]
      },
      {
        id: 2,
        title: 'Nike Default',
        descr: 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла")',
        img: require('../assets/img/4.png'),
        gallery: [
          {
            name: 'Nike boots First',
            img: require('../assets/img/4.png'),
          },
          {
            name: 'Nike boots Second',
            img: require('../assets/img/5.png'),
          },
          {
            name: 'Nike boots Third',
            img: require('../assets/img/6.png'),
          }
        ]
      },
      {
        id: 3,
        title: 'Nike Green',
        descr: 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла")',
        img: require('../assets/img/7.png'),
        gallery: [
          {
            name: 'Nike boots First',
            img: require('../assets/img/7.png'),
          },
          {
            name: 'Nike boots Second',
            img: require('../assets/img/8.png'),
          },
          {
            name: 'Nike boots Third',
            img: require('../assets/img/9.png'),
          }
        ]
      },
      {
        id: 4,
        title: 'Nike Street',
        descr: 'Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, "consectetur", и занялся его поисками в классической латинской литературе. В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги "de Finibus Bonorum et Malorum" ("О пределах добра и зла")',
        img: require('../assets/img/10.png'),
        gallery: [
          {
            name: 'Nike boots First',
            img: require('../assets/img/10.png'),
          },
          {
            name: 'Nike boots Second',
            img: require('../assets/img/11.png'),
          },
          {
            name: 'Nike boots Third',
            img: require('../assets/img/12.png'),
          }
        ]
      }
    ]
  },
  mutations: {

  },
  actions: {

  },
  getters: {
    getShopList (state) {
      return state.shopList
    },
    getProduct: (state) => (id) => {
      return state.shopList.find(product => product.id == id)
    }
  }
}
