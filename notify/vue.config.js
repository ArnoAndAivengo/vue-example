// on server with '/' or local ''

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  assetsDir: 'assets',
  productionSourceMap: false,
}
