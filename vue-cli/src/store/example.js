

export default{
  state: {
    message: 'hello vuex 1222223'
  },
  actions: {
    setMessage ({ commit }, payload) {
      commit('setMessage', payload)
    }
  },
  mutations: {
    setMessage (state, message) {
      state.message = message
    }
  },
  getters: {
    getMessage (state) {
      return state.message
    }
  }
}
