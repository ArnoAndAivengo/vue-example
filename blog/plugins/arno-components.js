import Vue from 'vue'

// UI
import Message from '@/components/UI/Message.vue'
import Intro from '~/components/UI/Intro.vue'
import PostsLists from '~/components/Blog/PostsList.vue'
// Controls
import ArnoButton from '@/components/UI/Controls/Button.vue'
import ArnoTextArea from '@/components/UI/Controls/TextArea.vue'
import ArnoInput from '@/components/UI/Controls/Input.vue'


// UI
Vue.component('Message', Message)
Vue.component('Intro', Intro)
Vue.component('PostsLists', PostsLists)
// Controls
Vue.component('ArnoButton', ArnoButton)
Vue.component('ArnoTextArea', ArnoTextArea)
Vue.component('ArnoInput', ArnoInput)


